#include <iostream> 
#include <ctime> 
using namespace std;
void printm(double** mat, int n, int m)
{
	for (int i = 0; i<n; ++i)
	{
		for (int j = 0; j<m; ++j)
		{
			cout << mat[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}
int main(int argc, char**argv)
{
	int n, m, q;
	n = atoi(argv[1]);
	m = atoi(argv[2]);
	q = atoi(argv[3]);
	srand(time(NULL));
	double **matrx1 = new double*[n];
	for (int i = 0; i<n; ++i)
	{
		matrx1[i] = new double[m];
		for (int j = 0; j<m; ++j)
			matrx1[i][j] = rand() % 9 + 1;
	}
	double **matrx2 = new double*[m];
	for (int i = 0; i<m; ++i)
	{
		matrx2[i] = new double[q];
		for (int j = 0; j<q; ++j)
			matrx2[i][j] = rand() % 9 + 1;
	}
	double **matrx3 = new double*[n];
	for (int i = 0; i < n; ++i)
	{
		matrx3[i] = new double[q];
		for (int j = 0; j < q; ++j)
		{
			matrx3[i][j] = 0;
			for (int k = 0; k < m; ++k)
				matrx3[i][j] += matrx1[i][k] * matrx2[k][j];
		}
	}
	printm(matrx1, n, m);
	printm(matrx2, m, q);
	printm(matrx3, n, q);
	for (int i = 0; i < n; ++i)
	{
		delete[]matrx1[i];
		matrx2[i];
		matrx3[i];
	}
	delete[]matrx1;
	delete[]matrx2;
	delete[]matrx3;
	system("pause");
}